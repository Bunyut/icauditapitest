﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class VfVerifyContractAllImage
    {
        public int Id { get; set; }
        public DateTime? LotDate { get; set; }
        public string ContractNo { get; set; }
        public string DocNo { get; set; }
        public byte IsBondsman { get; set; }
        public string ImagePath { get; set; }
        public string ImageName { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
