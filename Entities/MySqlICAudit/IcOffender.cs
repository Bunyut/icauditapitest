﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class IcOffender
    {
        public int Id { get; set; }
        public int IcAuditStatusId { get; set; }
        public int IcDoubtContractId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? FinalApprovedDate { get; set; }
        public string FinalApprovedBy { get; set; }
    }
}
