﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class ic_auditContext : DbContext
    {
        public ic_auditContext()
        {
        }

        public ic_auditContext(DbContextOptions<ic_auditContext> options)
            : base(options)
        {
        }

        public virtual DbSet<IcAuditAction> IcAuditAction { get; set; }
        public virtual DbSet<IcAuditChoice> IcAuditChoice { get; set; }
        public virtual DbSet<IcAuditCommittee> IcAuditCommittee { get; set; }
        public virtual DbSet<IcAuditFault> IcAuditFault { get; set; }
        public virtual DbSet<IcAuditPenalty> IcAuditPenalty { get; set; }
        public virtual DbSet<IcAuditStatus> IcAuditStatus { get; set; }
        public virtual DbSet<IcAuditStatusHistory> IcAuditStatusHistory { get; set; }
        public virtual DbSet<IcAuditTransaction> IcAuditTransaction { get; set; }
        public virtual DbSet<IcAuditUploadFiles> IcAuditUploadFiles { get; set; }
        public virtual DbSet<IcAuditUploadFilesGroup> IcAuditUploadFilesGroup { get; set; }
        public virtual DbSet<IcDoubtContract> IcDoubtContract { get; set; }
        public virtual DbSet<IcFiledocument> IcFiledocument { get; set; }
        public virtual DbSet<IcFlowStatus> IcFlowStatus { get; set; }
        public virtual DbSet<IcIncedent> IcIncedent { get; set; }
        public virtual DbSet<IcIncedentType> IcIncedentType { get; set; }
        public virtual DbSet<IcLoggerApp> IcLoggerApp { get; set; }
        public virtual DbSet<IcLoggerData> IcLoggerData { get; set; }
        public virtual DbSet<IcOffender> IcOffender { get; set; }
        public virtual DbSet<IcOffenderDetail> IcOffenderDetail { get; set; }
        public virtual DbSet<IcReplydocument> IcReplydocument { get; set; }
        public virtual DbSet<IcReport> IcReport { get; set; }
        public virtual DbSet<IcSenddocument> IcSenddocument { get; set; }
        public virtual DbSet<IcUser> IcUser { get; set; }
        public virtual DbSet<VfVerifyContract> VfVerifyContract { get; set; }
        public virtual DbSet<VfVerifyContractAllImage> VfVerifyContractAllImage { get; set; }
        public virtual DbSet<VfVerifyContractDetail> VfVerifyContractDetail { get; set; }
        public virtual DbSet<VfVerifyContractUser> VfVerifyContractUser { get; set; }
        public virtual DbSet<VfVerifyLoadImageContract> VfVerifyLoadImageContract { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySQL("server=192.170.2.22;user=k2service;password=k2serviceP@ssw0rd;database=ic_audit");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IcAuditAction>(entity =>
            {
                entity.ToTable("ic_audit_action", "ic_audit");

                entity.Property(e => e.Id)
                    .HasColumnType("tinyint(3) unsigned")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AuditActionName)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.RecordStatus)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("100");
            });

            modelBuilder.Entity<IcAuditChoice>(entity =>
            {
                entity.ToTable("ic_audit_choice", "ic_audit");

                entity.Property(e => e.Id)
                    .HasColumnType("tinyint(3) unsigned")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AuditChoiceName)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.RecordStatus)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("100");
            });

            modelBuilder.Entity<IcAuditCommittee>(entity =>
            {
                entity.ToTable("ic_audit_committee", "ic_audit");

                entity.Property(e => e.Id)
                    .HasColumnType("tinyint(3) unsigned")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CommitteeName)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.CommitteePosition)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasDefaultValueSql("100");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.RecordStatus)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("100");
            });

            modelBuilder.Entity<IcAuditFault>(entity =>
            {
                entity.ToTable("ic_audit_fault", "ic_audit");

                entity.Property(e => e.Id)
                    .HasColumnType("tinyint(3) unsigned")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.AuditFaultName)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.RecordStatus)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("100");
            });

            modelBuilder.Entity<IcAuditPenalty>(entity =>
            {
                entity.ToTable("ic_audit_penalty", "ic_audit");

                entity.Property(e => e.Id).HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.AuditPenaltyName)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.RecordStatus)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("100");
            });

            modelBuilder.Entity<IcAuditStatus>(entity =>
            {
                entity.ToTable("ic_audit_status", "ic_audit");

                entity.HasIndex(e => e.ContractNo)
                    .HasName("ContractNo");

                entity.HasIndex(e => e.IcDoubtContractId)
                    .HasName("IcDoubtContractId");

                entity.HasIndex(e => e.IsCorruption)
                    .HasName("IsCorruption");

                entity.HasIndex(e => e.JobsType)
                    .HasName("JobsType");

                entity.HasIndex(e => e.SourceType)
                    .HasName("SourceType");

                entity.HasIndex(e => e.VerifyStatus)
                    .HasName("VerifyStatus");

                entity.Property(e => e.Id).HasColumnType("int(11) unsigned");

                entity.Property(e => e.AbnormalCarBorrowMemo).IsUnicode(false);

                entity.Property(e => e.AbnormalCarSeiezedMemo).IsUnicode(false);

                entity.Property(e => e.AbnormalContractMemo).IsUnicode(false);

                entity.Property(e => e.AbnormalIssue).IsUnicode(false);

                entity.Property(e => e.ApprovedBy)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.AuditDate).HasColumnType("date");

                entity.Property(e => e.BranchNpl)
                    .HasColumnName("BranchNPL")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CarBorrowCondition)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CarSeiezedCondition)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Ceosummary)
                    .HasColumnName("CEOSummary")
                    .IsUnicode(false);

                entity.Property(e => e.CommitteePunishment).IsUnicode(false);

                entity.Property(e => e.ContractApprove)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ContractCondition)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ContractInvestigateMemo).IsUnicode(false);

                entity.Property(e => e.ContractNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContractNormal)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ContractNotAllowMemo).IsUnicode(false);

                entity.Property(e => e.ContractTrace)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ContractTraceMemo).IsUnicode(false);

                entity.Property(e => e.Cposummary)
                    .HasColumnName("CPOSummary")
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Crosummary)
                    .HasColumnName("CROSummary")
                    .IsUnicode(false);

                entity.Property(e => e.IcDoubtContractId).HasColumnType("int(11)");

                entity.Property(e => e.IncedentType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.InvestigateComment).IsUnicode(false);

                entity.Property(e => e.IsCorruption)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.JobsType).HasColumnType("int(11)");

                entity.Property(e => e.OriginalAffairExplain).IsUnicode(false);

                entity.Property(e => e.OriginalAffairNotice).IsUnicode(false);

                entity.Property(e => e.OriginalAffairPrevent).IsUnicode(false);

                entity.Property(e => e.OriginalAffairSuggest).IsUnicode(false);

                entity.Property(e => e.SourceType)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.SummaryFileUpload).IsUnicode(false);

                entity.Property(e => e.VerifyStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.WrongConditionMemo).IsUnicode(false);
            });

            modelBuilder.Entity<IcAuditStatusHistory>(entity =>
            {
                entity.ToTable("ic_audit_status_history", "ic_audit");

                entity.HasIndex(e => e.ContractNo)
                    .HasName("ContractNo");

                entity.HasIndex(e => e.HistoryDate)
                    .HasName("HistoryDate");

                entity.HasIndex(e => e.IcDoubtContractId)
                    .HasName("IcDoubtContractId");

                entity.HasIndex(e => e.IsCorruption)
                    .HasName("IsCorruption");

                entity.HasIndex(e => e.JobsType)
                    .HasName("JobsType");

                entity.HasIndex(e => e.RecordType)
                    .HasName("RecordType");

                entity.HasIndex(e => e.SourceType)
                    .HasName("SourceType");

                entity.HasIndex(e => e.VerifyStatus)
                    .HasName("VerifyStatus");

                entity.Property(e => e.Id).HasColumnType("int(11) unsigned");

                entity.Property(e => e.AbnormalCarBorrowMemo).IsUnicode(false);

                entity.Property(e => e.AbnormalCarSeiezedMemo).IsUnicode(false);

                entity.Property(e => e.AbnormalContractMemo).IsUnicode(false);

                entity.Property(e => e.AbnormalIssue).IsUnicode(false);

                entity.Property(e => e.ApprovedBy)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.BranchNpl)
                    .HasColumnName("BranchNPL")
                    .HasColumnType("decimal(10,2)");

                entity.Property(e => e.CarBorrowCondition)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.CarSeiezedCondition)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Ceosummary)
                    .HasColumnName("CEOSummary")
                    .IsUnicode(false);

                entity.Property(e => e.CommitteePunishment).IsUnicode(false);

                entity.Property(e => e.ContractApprove)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ContractCondition)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ContractInvestigateMemo).IsUnicode(false);

                entity.Property(e => e.ContractNo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContractNormal)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ContractNotAllowMemo).IsUnicode(false);

                entity.Property(e => e.ContractTrace)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ContractTraceMemo).IsUnicode(false);

                entity.Property(e => e.Cposummary)
                    .HasColumnName("CPOSummary")
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Crosummary)
                    .HasColumnName("CROSummary")
                    .IsUnicode(false);

                entity.Property(e => e.HistoryDate).HasColumnType("date");

                entity.Property(e => e.IcAuditStatusId).HasColumnType("int(10) unsigned");

                entity.Property(e => e.IcDoubtContractId).HasColumnType("int(11)");

                entity.Property(e => e.IncedentType)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.InvestigateComment).IsUnicode(false);

                entity.Property(e => e.IsCorruption)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.JobsType).HasColumnType("int(11)");

                entity.Property(e => e.OriginalAffairExplain).IsUnicode(false);

                entity.Property(e => e.OriginalAffairNotice).IsUnicode(false);

                entity.Property(e => e.OriginalAffairPrevent).IsUnicode(false);

                entity.Property(e => e.OriginalAffairSuggest).IsUnicode(false);

                entity.Property(e => e.RecordType)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SourceType)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.SummaryFileUpload).IsUnicode(false);

                entity.Property(e => e.VerifyStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.WrongConditionMemo).IsUnicode(false);
            });

            modelBuilder.Entity<IcAuditTransaction>(entity =>
            {
                entity.ToTable("ic_audit_transaction", "ic_audit");

                entity.HasIndex(e => e.IcAuditStatusId)
                    .HasName("IcAuditStatusId");

                entity.HasIndex(e => e.IcDoubtContractId)
                    .HasName("IcDoubtContractId");

                entity.HasIndex(e => e.LogId)
                    .HasName("LogId");

                entity.Property(e => e.Id).HasColumnType("int(11) unsigned");

                entity.Property(e => e.ActionByAduserName)
                    .IsRequired()
                    .HasColumnName("ActionByADUserName")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ActionByFullName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IcAuditStatusId)
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IcDoubtContractId)
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IncendentId)
                    .HasColumnType("int(11) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.LogDate).HasColumnType("date");

                entity.Property(e => e.LogId).HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.LogName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SourceType)
                    .HasColumnType("tinyint(3) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<IcAuditUploadFiles>(entity =>
            {
                entity.ToTable("ic_audit_upload_files", "ic_audit");

                entity.HasIndex(e => e.ContractNo)
                    .HasName("ContractNo");

                entity.HasIndex(e => new { e.RefTableName, e.RefTableId })
                    .HasName("RefTableName");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.ContractNo)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreateEmp)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerNo).HasColumnType("int(11)");

                entity.Property(e => e.FileName)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FilePath)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FileSize)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FileType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.GroupId).HasColumnType("int(11)");

                entity.Property(e => e.RefTableId).HasColumnType("int(11)");

                entity.Property(e => e.RefTableName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<IcAuditUploadFilesGroup>(entity =>
            {
                entity.ToTable("ic_audit_upload_files_group", "ic_audit");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreateDate).HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.CreateEmp)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.GroupName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.GroupStatus)
                    .HasColumnType("int(3)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.GroupType)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<IcDoubtContract>(entity =>
            {
                entity.ToTable("ic_doubt_contract", "ic_audit");

                entity.HasIndex(e => e.Area)
                    .HasName("Area");

                entity.HasIndex(e => e.AssignTo)
                    .HasName("AssignTo");

                entity.HasIndex(e => e.BranchCode)
                    .HasName("BranchCode");

                entity.HasIndex(e => e.ContractNo)
                    .HasName("ContractNo");

                entity.HasIndex(e => e.JobsType)
                    .HasName("JobsType");

                entity.HasIndex(e => e.K2processId)
                    .HasName("K2ProcessId");

                entity.HasIndex(e => e.ProvinceCode)
                    .HasName("ProvinceCode");

                entity.HasIndex(e => e.Region)
                    .HasName("Region");

                entity.HasIndex(e => e.StatusDate)
                    .HasName("StatusDate");

                entity.HasIndex(e => e.VerifyStatus)
                    .HasName("VerifyStatus");

                entity.Property(e => e.Id).HasColumnType("int(11) unsigned");

                entity.Property(e => e.AbnormalIssue).IsUnicode(false);

                entity.Property(e => e.AgingGroupId).HasColumnType("int(11)");

                entity.Property(e => e.AgingGroupName)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.AppointmentsDate).HasColumnType("date");

                entity.Property(e => e.AppointmentsMemo).IsUnicode(false);

                entity.Property(e => e.Area)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AreaName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AssignByLv5)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.AssignByLv6)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.AssignTo)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.AssignToLv5)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.BranchCode)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.BranchName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CancelBy)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CancelReason)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CarSeiezedCost)
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.CommissionVendorAd)
                    .HasColumnName("CommissionVendorAD")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.ContractCost)
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.ContractInsuranceAmt)
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.ContractMonth).HasColumnType("int(11)");

                entity.Property(e => e.ContractMonthAmt)
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.ContractMonthAmt2)
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.ContractNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContractNoOriginal)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContractProfitRate)
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.ContractProfitRate2)
                    .HasColumnType("decimal(7,4)")
                    .HasDefaultValueSql("0.0000");

                entity.Property(e => e.ContractType)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ContractTypeName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CostRemain)
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CurrentFlowStatus)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerAddress)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerAddress2)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerCoordinates)
                    .HasMaxLength(55)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerCoordinates2)
                    .HasMaxLength(55)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerDistrict)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerDistrict2)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerJob)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerJob2)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerProvince)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerProvince2)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerSubDistrict)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CustomerSubDistrict2)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.DatePeriodPostSale)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DebtCostRemain)
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.DueInMonth).HasColumnType("date");

                entity.Property(e => e.DurationProsecution)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.FollowChecked).HasColumnType("tinyint(2)");

                entity.Property(e => e.Gurantee).HasColumnType("tinyint(4)");

                entity.Property(e => e.JobsType)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.K2processId)
                    .HasColumnName("K2ProcessId")
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.LastPay).HasColumnType("date");

                entity.Property(e => e.LitigateStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ManagerAd)
                    .HasColumnName("ManagerAD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.NoticeStatus).HasColumnType("int(11)");

                entity.Property(e => e.PaidPeriod).HasColumnType("int(11)");

                entity.Property(e => e.PayStatus)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.PeriodMustPay).HasColumnType("int(11)");

                entity.Property(e => e.PeriodMustPayRange)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PeriodPay).HasColumnType("int(11)");

                entity.Property(e => e.PeriodPayRange)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PriceList)
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.ProductBrand)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProductModel)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ProductModelDetail)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ProductYear)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.ProfitPercent)
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.Province)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProvinceCode)
                    .IsRequired()
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.RecordStatus)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("100");

                entity.Property(e => e.Region)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RegionName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SalePrice)
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.SalePriceFinal)
                    .HasColumnType("decimal(10,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.SourceType)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.StaffAssetCarPriceAd)
                    .HasColumnName("StaffAssetCarPriceAD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.StaffCheckCreditAd)
                    .HasColumnName("StaffCheckCreditAD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.StaffSaleAd)
                    .HasColumnName("StaffSaleAD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.StaffWarroomAd)
                    .HasColumnName("StaffWarroomAD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.StatusDate).HasColumnType("date");

                entity.Property(e => e.VerifyStatus)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<IcFiledocument>(entity =>
            {
                entity.ToTable("ic_filedocument", "ic_audit");

                entity.HasIndex(e => e.DocumentReffId)
                    .HasName("DocumentReffId");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.DocumentContentType)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DocumentPath)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DocumentReffId)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.DocumentType)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<IcFlowStatus>(entity =>
            {
                entity.ToTable("ic_flow_status", "ic_audit");

                entity.HasIndex(e => e.FlowStatusType)
                    .HasName("FlowStatusType");

                entity.HasIndex(e => e.Sorting)
                    .HasName("Sorting");

                entity.HasIndex(e => e.StatusCode)
                    .HasName("StatusCode");

                entity.Property(e => e.Id)
                    .HasColumnType("tinyint(3) unsigned")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.FlowStatusName)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.FlowStatusType)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("100");

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.RecordStatus)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("100");

                entity.Property(e => e.Sorting)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("100");

                entity.Property(e => e.StatusCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<IcIncedent>(entity =>
            {
                entity.ToTable("ic_incedent", "ic_audit");

                entity.HasIndex(e => e.BranchNo)
                    .HasName("BranchNo");

                entity.HasIndex(e => e.DoubtId)
                    .HasName("DoubtId");

                entity.HasIndex(e => e.HrStatus)
                    .HasName("HrStatus");

                entity.HasIndex(e => e.IncedentTypeId)
                    .HasName("IncedentTypeId");

                entity.HasIndex(e => e.SyNo)
                    .HasName("SY_NO");

                entity.Property(e => e.Id).HasColumnType("int(11) unsigned");

                entity.Property(e => e.AmountCash)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AssignByLv5)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.AssignByLv6)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.AssignDetail).IsUnicode(false);

                entity.Property(e => e.AssignTo)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.AssignToLv5)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.BranchName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.BranchNo)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CancelAd)
                    .HasColumnName("CancelAD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CancelFullName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedAd)
                    .IsRequired()
                    .HasColumnName("CreatedAD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedFullName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DataFormAudit).IsUnicode(false);

                entity.Property(e => e.DataFromDoc).IsUnicode(false);

                entity.Property(e => e.DataFromIssue).IsUnicode(false);

                entity.Property(e => e.DepartmentRelated).IsUnicode(false);

                entity.Property(e => e.DocumentRelated).IsUnicode(false);

                entity.Property(e => e.DoubtId).HasColumnType("int(11)");

                entity.Property(e => e.HrStatus).HasColumnType("tinyint(1)");

                entity.Property(e => e.IncedentTypeId).HasColumnType("tinyint(3)");

                entity.Property(e => e.ModifiedAd)
                    .HasColumnName("ModifiedAD")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedFullName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecordStatus).HasColumnType("tinyint(3)");

                entity.Property(e => e.RequestDate).HasColumnType("date");

                entity.Property(e => e.RequestDetail).HasColumnType("mediumtext");

                entity.Property(e => e.RequestTitle)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SyNo)
                    .HasColumnName("SY_NO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WorkDetail).IsUnicode(false);
            });

            modelBuilder.Entity<IcIncedentType>(entity =>
            {
                entity.ToTable("ic_incedent_type", "ic_audit");

                entity.Property(e => e.Id).HasColumnType("tinyint(3) unsigned");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.IncendentTypeName)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.RecordStatus).HasColumnType("tinyint(3)");
            });

            modelBuilder.Entity<IcLoggerApp>(entity =>
            {
                entity.ToTable("ic_logger_app", "ic_audit");

                entity.Property(e => e.Id).HasColumnType("int(11) unsigned");

                entity.Property(e => e.ActionByAduserName)
                    .HasColumnName("ActionByADUserName")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ActionByFullName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.AppName)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ErrorCode)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.ErrorMessage).IsUnicode(false);

                entity.Property(e => e.FunctionName)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.LogDate).HasColumnType("date");

                entity.Property(e => e.ReqDescription).IsUnicode(false);
            });

            modelBuilder.Entity<IcLoggerData>(entity =>
            {
                entity.ToTable("ic_logger_data", "ic_audit");

                entity.Property(e => e.Id).HasColumnType("int(11) unsigned");

                entity.Property(e => e.ActionByAduserName)
                    .IsRequired()
                    .HasColumnName("ActionByADUserName")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ActionByFullName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ActionType)
                    .IsRequired()
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.AppName)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.FieldName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FunctionName)
                    .IsRequired()
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.LogDate).HasColumnType("date");

                entity.Property(e => e.OldData).IsUnicode(false);

                entity.Property(e => e.RecordId).HasColumnType("int(11)");

                entity.Property(e => e.ReplaceData).IsUnicode(false);

                entity.Property(e => e.TableName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<IcOffender>(entity =>
            {
                entity.ToTable("ic_offender", "ic_audit");

                entity.HasIndex(e => e.IcAuditStatusId)
                    .HasName("IcAuditStatusId");

                entity.HasIndex(e => e.IcDoubtContractId)
                    .HasName("IcDoubtContractId");

                entity.Property(e => e.Id).HasColumnType("int(11) unsigned");

                entity.Property(e => e.ApprovedBy)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.FinalApprovedBy)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.IcAuditStatusId).HasColumnType("int(11)");

                entity.Property(e => e.IcDoubtContractId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<IcOffenderDetail>(entity =>
            {
                entity.ToTable("ic_offender_detail", "ic_audit");

                entity.HasIndex(e => e.AdUserLogin)
                    .HasName("AdUserLogin");

                entity.HasIndex(e => e.IcAuditFaultId)
                    .HasName("IcAuditFaultId");

                entity.HasIndex(e => e.IcAuditPenaltyId)
                    .HasName("IcAuditPenaltyId");

                entity.HasIndex(e => e.IcOffenderId)
                    .HasName("IcOffenderId");

                entity.Property(e => e.Id).HasColumnType("int(11) unsigned");

                entity.Property(e => e.AdUserLogin)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeName)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.EmployeeWorkAges).HasColumnType("decimal(10,2)");

                entity.Property(e => e.IcAuditFaultId).HasColumnType("tinyint(3)");

                entity.Property(e => e.IcAuditFaultName)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.IcAuditPenaltyId).HasColumnType("tinyint(3)");

                entity.Property(e => e.IcAuditPenaltyName)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.IcOffenderId).HasColumnType("int(11) unsigned");

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecordStatus).HasColumnType("tinyint(3)");
            });

            modelBuilder.Entity<IcReplydocument>(entity =>
            {
                entity.ToTable("ic_replydocument", "ic_audit");

                entity.HasIndex(e => e.DocumentNo)
                    .HasName("DocumentNo")
                    .IsUnique();

                entity.HasIndex(e => e.IcSendDocumentId)
                    .HasName("IcSendDocumentId");

                entity.HasIndex(e => e.RunningMonth)
                    .HasName("RunningMonth");

                entity.HasIndex(e => e.RunningYear)
                    .HasName("RunningYear");

                entity.Property(e => e.Id).HasColumnType("int(10) unsigned");

                entity.Property(e => e.Content)
                    .IsRequired()
                    .HasColumnType("mediumtext");

                entity.Property(e => e.CreatedAd)
                    .IsRequired()
                    .HasColumnName("CreatedAD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("date");

                entity.Property(e => e.CreatedFullName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentDate).HasColumnType("date");

                entity.Property(e => e.DocumentNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IcSendDocumentId).HasColumnType("int(11)");

                entity.Property(e => e.OpenDateTime).HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.OpenStatus)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.PositionName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecordStatus)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("100");

                entity.Property(e => e.ReplyAd)
                    .IsRequired()
                    .HasColumnName("ReplyAD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReplyEmail)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReplyFullName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RunningId)
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.RunningMonth)
                    .HasColumnType("tinyint(3) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.RunningYear)
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Subject)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Topic)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<IcReport>(entity =>
            {
                entity.ToTable("ic_report", "ic_audit");

                entity.HasIndex(e => e.ReportNo)
                    .HasName("DocumentNo")
                    .IsUnique();

                entity.HasIndex(e => e.RunningMonth)
                    .HasName("RunningMonth");

                entity.HasIndex(e => e.RunningYear)
                    .HasName("RunningYear");

                entity.Property(e => e.Id).HasColumnType("int(10) unsigned");

                entity.Property(e => e.CreatedAd)
                    .IsRequired()
                    .HasColumnName("CreatedAD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("date");

                entity.Property(e => e.CreatedFullName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecordStatus)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("100");

                entity.Property(e => e.ReportName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReportNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RunningId)
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.RunningMonth)
                    .HasColumnType("tinyint(3) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.RunningYear)
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<IcSenddocument>(entity =>
            {
                entity.ToTable("ic_senddocument", "ic_audit");

                entity.HasIndex(e => e.ContractNo)
                    .HasName("ContractNo");

                entity.HasIndex(e => e.DocumentNo)
                    .HasName("DocumentNo")
                    .IsUnique();

                entity.HasIndex(e => e.IcAuditStatusId)
                    .HasName("IcAuditStatusId");

                entity.HasIndex(e => e.IcDoubtContractId)
                    .HasName("IcDoubtContractId");

                entity.HasIndex(e => e.RunningMonth)
                    .HasName("RunningMonth");

                entity.HasIndex(e => e.RunningYear)
                    .HasName("RunningYear");

                entity.Property(e => e.Id).HasColumnType("int(10) unsigned");

                entity.Property(e => e.Content)
                    .IsRequired()
                    .HasColumnType("mediumtext");

                entity.Property(e => e.ContractNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedAd)
                    .IsRequired()
                    .HasColumnName("CreatedAD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("date");

                entity.Property(e => e.CreatedFullName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DocumentDate).HasColumnType("date");

                entity.Property(e => e.DocumentNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IcAuditStatusId).HasColumnType("int(11)");

                entity.Property(e => e.IcDoubtContractId).HasColumnType("int(11)");

                entity.Property(e => e.OpenDateTime).HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.OpenStatus)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.PositionName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReceiveAd)
                    .IsRequired()
                    .HasColumnName("ReceiveAD")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReceiveEmail)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReceiveFullName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecordStatus)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("100");

                entity.Property(e => e.RunningId)
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.RunningMonth)
                    .HasColumnType("tinyint(3) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.RunningYear)
                    .HasColumnType("int(10) unsigned")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Subject)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Topic)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<IcUser>(entity =>
            {
                entity.ToTable("ic_user", "ic_audit");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.RecordStatus)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("100");

                entity.Property(e => e.UserDisplayName)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.UserLevel).HasColumnType("int(11)");

                entity.Property(e => e.UserLoginId)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VfVerifyContract>(entity =>
            {
                entity.HasKey(e => e.VerifyContractId);

                entity.ToTable("vf_verify_contract", "ic_audit");

                entity.HasIndex(e => e.ContractNo)
                    .HasName("ContractNo");

                entity.HasIndex(e => e.LotDate)
                    .HasName("LotDate");

                entity.Property(e => e.VerifyContractId)
                    .HasColumnName("VerifyContractID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ContractNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.IsUploadSuccess)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IsVerify)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.LotDate).HasColumnType("date");
            });

            modelBuilder.Entity<VfVerifyContractAllImage>(entity =>
            {
                entity.ToTable("vf_verify_contract_all_image", "ic_audit");

                entity.HasIndex(e => e.ContractNo)
                    .HasName("ContractNo");

                entity.HasIndex(e => e.LotDate)
                    .HasName("LotDate");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ContractNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DocNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ImageName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ImagePath)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.IsBondsman)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.LotDate).HasColumnType("date");
            });

            modelBuilder.Entity<VfVerifyContractDetail>(entity =>
            {
                entity.ToTable("vf_verify_contract_detail", "ic_audit");

                entity.HasIndex(e => e.ContractNo)
                    .HasName("ContractNo");

                entity.HasIndex(e => e.IsAccurate)
                    .HasName("IsAccurate");

                entity.HasIndex(e => e.IsVerify)
                    .HasName("IsVerify");

                entity.Property(e => e.Id).HasColumnType("int(11) unsigned");

                entity.Property(e => e.AssetbookComment).IsUnicode(false);

                entity.Property(e => e.AssetbookIsAccurate).HasColumnType("tinyint(3)");

                entity.Property(e => e.CancelBy)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CancelReason)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CommitComment).IsUnicode(false);

                entity.Property(e => e.CommitIsAccurate).HasColumnType("tinyint(3)");

                entity.Property(e => e.ContactPersonComment).IsUnicode(false);

                entity.Property(e => e.ContactPersonIsAccurate).HasColumnType("tinyint(3)");

                entity.Property(e => e.ContractNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ContractPage1Comment).IsUnicode(false);

                entity.Property(e => e.ContractPage1IsAccurate).HasColumnType("tinyint(3)");

                entity.Property(e => e.ContractPage2Comment).IsUnicode(false);

                entity.Property(e => e.ContractPage2IsAccurate).HasColumnType("tinyint(3)");

                entity.Property(e => e.ContractPage3Comment).IsUnicode(false);

                entity.Property(e => e.ContractPage3IsAccurate).HasColumnType("tinyint(3)");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.GuaranteeComment).IsUnicode(false);

                entity.Property(e => e.GuaranteeIsAccurate).HasColumnType("tinyint(3)");

                entity.Property(e => e.HouseRegistrationComment).IsUnicode(false);

                entity.Property(e => e.HouseRegistrationIsAccurate).HasColumnType("tinyint(3)");

                entity.Property(e => e.IdcardComment)
                    .HasColumnName("IDCardComment")
                    .IsUnicode(false);

                entity.Property(e => e.IdcardIsAccurate)
                    .HasColumnName("IDCardIsAccurate")
                    .HasColumnType("tinyint(3)");

                entity.Property(e => e.IsAccurate)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IsVerify)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VfVerifyContractUser>(entity =>
            {
                entity.ToTable("vf_verify_contract_user", "ic_audit");

                entity.HasIndex(e => e.UserLoginId)
                    .HasName("UserLoginId");

                entity.Property(e => e.Id)
                    .HasColumnType("tinyint(3) unsigned")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.RecordStatus)
                    .HasColumnType("tinyint(3)")
                    .HasDefaultValueSql("100");

                entity.Property(e => e.UserDisplayName)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UserLoginId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<VfVerifyLoadImageContract>(entity =>
            {
                entity.ToTable("vf_verify_load_image_contract", "ic_audit");

                entity.HasIndex(e => e.ContractNo)
                    .HasName("ContractNo");

                entity.HasIndex(e => e.DestinationFilePath)
                    .HasName("DestinationFilePath");

                entity.HasIndex(e => e.LotDate)
                    .HasName("LotDate");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ContractNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DestinationFilePath)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.HaveInSystem)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.ImageName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IsReplace)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IsUploadSuccess)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.LotDate).HasColumnType("date");

                entity.Property(e => e.ReplacePath)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.SourceFilePath)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);
            });
        }
    }
}
