﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class IcAuditTransaction
    {
        public int Id { get; set; }
        public int IcAuditStatusId { get; set; }
        public int IcDoubtContractId { get; set; }
        public int IncendentId { get; set; }
        public byte SourceType { get; set; }
        public string ActionByAduserName { get; set; }
        public string ActionByFullName { get; set; }
        public byte LogId { get; set; }
        public string LogName { get; set; }
        public DateTime LogDate { get; set; }
        public TimeSpan LogTime { get; set; }
    }
}
