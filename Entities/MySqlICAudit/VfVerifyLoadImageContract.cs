﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class VfVerifyLoadImageContract
    {
        public int Id { get; set; }
        public DateTime LotDate { get; set; }
        public string ContractNo { get; set; }
        public byte HaveInSystem { get; set; }
        public byte IsUploadSuccess { get; set; }
        public byte IsReplace { get; set; }
        public string SourceFilePath { get; set; }
        public string DestinationFilePath { get; set; }
        public string ReplacePath { get; set; }
        public string ImageName { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
