﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class IcOffenderDetail
    {
        public int Id { get; set; }
        public int IcOffenderId { get; set; }
        public string AdUserLogin { get; set; }
        public string EmployeeName { get; set; }
        public byte IcAuditFaultId { get; set; }
        public string IcAuditFaultName { get; set; }
        public byte? IcAuditPenaltyId { get; set; }
        public string IcAuditPenaltyName { get; set; }
        public decimal? EmployeeWorkAges { get; set; }
        public byte RecordStatus { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
