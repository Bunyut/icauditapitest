﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class VfVerifyContract
    {
        public int VerifyContractId { get; set; }
        public string ContractNo { get; set; }
        public DateTime? LotDate { get; set; }
        public byte IsVerify { get; set; }
        public byte IsUploadSuccess { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
