﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class VfVerifyContractDetail
    {
        public int Id { get; set; }
        public string ContractNo { get; set; }
        public byte? IsVerify { get; set; }
        public byte? IsAccurate { get; set; }
        public string ContractPage1Comment { get; set; }
        public byte? ContractPage1IsAccurate { get; set; }
        public string ContractPage2Comment { get; set; }
        public byte? ContractPage2IsAccurate { get; set; }
        public string ContractPage3Comment { get; set; }
        public byte? ContractPage3IsAccurate { get; set; }
        public string CommitComment { get; set; }
        public byte? CommitIsAccurate { get; set; }
        public string AssetbookComment { get; set; }
        public byte? AssetbookIsAccurate { get; set; }
        public string IdcardComment { get; set; }
        public byte? IdcardIsAccurate { get; set; }
        public string HouseRegistrationComment { get; set; }
        public byte? HouseRegistrationIsAccurate { get; set; }
        public string ContactPersonComment { get; set; }
        public byte? ContactPersonIsAccurate { get; set; }
        public string GuaranteeComment { get; set; }
        public byte? GuaranteeIsAccurate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CancelDate { get; set; }
        public string CancelBy { get; set; }
        public string CancelReason { get; set; }
    }
}
