﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class IcLoggerData
    {
        public int Id { get; set; }
        public string AppName { get; set; }
        public string ActionType { get; set; }
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public int RecordId { get; set; }
        public string OldData { get; set; }
        public string ReplaceData { get; set; }
        public string FunctionName { get; set; }
        public string ActionByAduserName { get; set; }
        public string ActionByFullName { get; set; }
        public DateTime LogDate { get; set; }
        public TimeSpan LogTime { get; set; }
    }
}
