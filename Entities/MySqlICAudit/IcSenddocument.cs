﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class IcSenddocument
    {
        public int Id { get; set; }
        public string DocumentNo { get; set; }
        public int RunningId { get; set; }
        public int IcAuditStatusId { get; set; }
        public int IcDoubtContractId { get; set; }
        public string ContractNo { get; set; }
        public byte RunningMonth { get; set; }
        public int RunningYear { get; set; }
        public DateTime DocumentDate { get; set; }
        public string ReceiveEmail { get; set; }
        public string ReceiveFullName { get; set; }
        public string ReceiveAd { get; set; }
        public string Topic { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string PositionName { get; set; }
        public string CreatedFullName { get; set; }
        public string CreatedAd { get; set; }
        public DateTime CreatedDate { get; set; }
        public TimeSpan CreatedTime { get; set; }
        public byte OpenStatus { get; set; }
        public DateTime OpenDateTime { get; set; }
        public byte RecordStatus { get; set; }
    }
}
