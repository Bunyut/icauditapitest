﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class IcUser
    {
        public int Id { get; set; }
        public string UserLoginId { get; set; }
        public string UserDisplayName { get; set; }
        public int? UserLevel { get; set; }
        public int? RecordStatus { get; set; }
    }
}
