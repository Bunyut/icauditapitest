﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class IcIncedent
    {
        public int Id { get; set; }
        public byte? IncedentTypeId { get; set; }
        public int? DoubtId { get; set; }
        public string RequestTitle { get; set; }
        public string RequestDetail { get; set; }
        public DateTime RequestDate { get; set; }
        public string BranchNo { get; set; }
        public string BranchName { get; set; }
        public string SyNo { get; set; }
        public byte RecordStatus { get; set; }
        public byte HrStatus { get; set; }
        public string AssignByLv6 { get; set; }
        public DateTime? AssignByLv6DateTime { get; set; }
        public string AssignToLv5 { get; set; }
        public string AssignByLv5 { get; set; }
        public DateTime? AssignByLv5DateTime { get; set; }
        public string AssignTo { get; set; }
        public string AssignDetail { get; set; }
        public string WorkDetail { get; set; }
        public string DataFormAudit { get; set; }
        public string DataFromDoc { get; set; }
        public string DataFromIssue { get; set; }
        public string DepartmentRelated { get; set; }
        public string DocumentRelated { get; set; }
        public string AmountCash { get; set; }
        public DateTime? AuditDate { get; set; }
        public string CreatedAd { get; set; }
        public string CreatedFullName { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedAd { get; set; }
        public string ModifiedFullName { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CancelAd { get; set; }
        public string CancelFullName { get; set; }
        public DateTime? CancelDate { get; set; }
    }
}
