﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class IcAuditStatus
    {
        public int Id { get; set; }
        public int IcDoubtContractId { get; set; }
        public int? JobsType { get; set; }
        public byte SourceType { get; set; }
        public string ContractNo { get; set; }
        public string ContractCondition { get; set; }
        public string WrongConditionMemo { get; set; }
        public string ContractNormal { get; set; }
        public string AbnormalContractMemo { get; set; }
        public string ContractApprove { get; set; }
        public string ContractNotAllowMemo { get; set; }
        public string ContractTrace { get; set; }
        public string ContractTraceMemo { get; set; }
        public string VerifyStatus { get; set; }
        public string IsCorruption { get; set; }
        public string ContractInvestigateMemo { get; set; }
        public string CarBorrowCondition { get; set; }
        public string AbnormalCarBorrowMemo { get; set; }
        public string CarSeiezedCondition { get; set; }
        public string AbnormalCarSeiezedMemo { get; set; }
        public string IncedentType { get; set; }
        public string AbnormalIssue { get; set; }
        public string InvestigateComment { get; set; }
        public decimal? BranchNpl { get; set; }
        public string CommitteePunishment { get; set; }
        public string OriginalAffairExplain { get; set; }
        public string OriginalAffairNotice { get; set; }
        public string OriginalAffairPrevent { get; set; }
        public string OriginalAffairSuggest { get; set; }
        public string Crosummary { get; set; }
        public string Cposummary { get; set; }
        public string Ceosummary { get; set; }
        public string SummaryFileUpload { get; set; }
        public DateTime? AuditDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ApprovedBy { get; set; }
        public DateTime? ApprovedDate { get; set; }
    }
}
