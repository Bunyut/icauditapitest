﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class IcAuditAction
    {
        public byte Id { get; set; }
        public string AuditActionName { get; set; }
        public byte RecordStatus { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
