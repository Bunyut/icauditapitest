﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class IcFlowStatus
    {
        public byte Id { get; set; }
        public string StatusCode { get; set; }
        public string FlowStatusName { get; set; }
        public byte FlowStatusType { get; set; }
        public byte Sorting { get; set; }
        public byte RecordStatus { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
