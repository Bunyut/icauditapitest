﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class IcFiledocument
    {
        public int Id { get; set; }
        public string DocumentType { get; set; }
        public string DocumentPath { get; set; }
        public string DocumentName { get; set; }
        public int DocumentReffId { get; set; }
        public string DocumentContentType { get; set; }
    }
}
