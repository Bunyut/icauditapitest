﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class IcAuditUploadFiles
    {
        public int Id { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public string FileSize { get; set; }
        public string RefTableName { get; set; }
        public int? RefTableId { get; set; }
        public string ContractNo { get; set; }
        public int? CustomerNo { get; set; }
        public int? GroupId { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateEmp { get; set; }
    }
}
