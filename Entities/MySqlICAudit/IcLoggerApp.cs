﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class IcLoggerApp
    {
        public int Id { get; set; }
        public string AppName { get; set; }
        public string FunctionName { get; set; }
        public string ReqDescription { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string ActionByAduserName { get; set; }
        public string ActionByFullName { get; set; }
        public DateTime? LogDate { get; set; }
        public TimeSpan? LogTime { get; set; }
    }
}
