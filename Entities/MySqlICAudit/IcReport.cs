﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class IcReport
    {
        public int Id { get; set; }
        public string ReportNo { get; set; }
        public int RunningId { get; set; }
        public byte RunningMonth { get; set; }
        public int RunningYear { get; set; }
        public string ReportName { get; set; }
        public string CreatedFullName { get; set; }
        public string CreatedAd { get; set; }
        public DateTime CreatedDate { get; set; }
        public TimeSpan CreatedTime { get; set; }
        public byte RecordStatus { get; set; }
    }
}
