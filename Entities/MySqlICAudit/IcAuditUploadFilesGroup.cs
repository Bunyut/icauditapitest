﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class IcAuditUploadFilesGroup
    {
        public int Id { get; set; }
        public string GroupName { get; set; }
        public string GroupType { get; set; }
        public int? GroupStatus { get; set; }
        public DateTime? CreateDate { get; set; }
        public string CreateEmp { get; set; }
    }
}
