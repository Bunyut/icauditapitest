﻿using System;
using System.Collections.Generic;

namespace IcAuditApiTest.Entities.MySqlICAudit
{
    public partial class IcAuditPenalty
    {
        public byte Id { get; set; }
        public string AuditPenaltyName { get; set; }
        public byte RecordStatus { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
