﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IcAuditApiTest.Entities.MySqlICAudit;
using IcAuditApiTest.Persists.MysqlIcAudit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace IcAuditApiTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FaultController : ControllerBase
    {
        private readonly AuditFaultPersist auditFaultPersist;
        public FaultController(ic_auditContext icAuditContext)
        {
            this.auditFaultPersist = new AuditFaultPersist(icAuditContext);
        }
        // GET: api/Fault
        [HttpGet]
        public IActionResult Get()
        {
            List<IcAuditFault> items = this.auditFaultPersist.GetAllAuditFault();
            return Ok(items);
        }

        // GET: api/Fault/5
        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(int id)
        {
            IcAuditFault item = this.auditFaultPersist.GetById(id);
            return Ok(item);
        }

        // POST: api/Fault
        [HttpPost]
        public IActionResult Post(IcAuditFault value)
        {
            IcAuditFault icAuditFault = new IcAuditFault();
            icAuditFault.AuditFaultName = value.AuditFaultName;
            icAuditFault.RecordStatus = 100;
            icAuditFault.CreatedBy = "99999";
            icAuditFault.CreatedDate = DateTime.Now;

            this.auditFaultPersist.Add(icAuditFault);
            this.auditFaultPersist.Save();

            return Ok(1);
        }

        // PUT: api/Fault/5
        [HttpPut("update")]
        public IActionResult Put(IcAuditFault value)
        {
            IcAuditFault icAuditFault = this.auditFaultPersist.GetById(value.Id);
            icAuditFault.AuditFaultName = value.AuditFaultName;

            this.auditFaultPersist.Edit(value.Id, icAuditFault);
            this.auditFaultPersist.Save();

            return Ok(1);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            IcAuditFault icAuditFault = this.auditFaultPersist.GetById(id);
            icAuditFault.RecordStatus = 99;

            this.auditFaultPersist.Edit(id, icAuditFault);
            this.auditFaultPersist.Save();

            return Ok(1);
        }
    }
}
