﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace IcAuditApiTest.Persists
{
    public abstract class BasePersist<T, U> where T : class where U : DbContext
    {
        #region Members
        protected DbContext context;
        private DbSet<T> dbSet;
        #endregion

        #region Contructors
        public BasePersist(DbContext context)
        {
            this.context = context;
            dbSet = this.context.Set<T>();
        }
        #endregion

        #region Methods
        public U GetContext()
        {
            return (U)context;
        }

        public IQueryable<T> Get()
        {
            return dbSet;
        }

        public T GetById(object id)
        {
            return dbSet.Find(id);
        }

        public void Add(T newObj)
        {
            dbSet.Add(newObj);
        }

        public int Count()
        {
            return dbSet.Count();
        }

        public void Edit(object id, T updateObj)
        {
            dbSet.Attach(updateObj);
            context.Entry(updateObj).State = EntityState.Modified;
        }

        public void Delete(object id, T deleteObject)
        {
            dbSet.Attach(deleteObject);
            context.Entry(deleteObject).State = EntityState.Deleted;
        }

        public void Save()
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbUpdateException ex)
            {
                throw new Exception(ex.InnerException.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
    }
}
