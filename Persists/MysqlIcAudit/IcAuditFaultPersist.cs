﻿using IcAuditApiTest.Entities.MySqlICAudit;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IcAuditApiTest.Persists.MysqlIcAudit
{
    public class AuditFaultPersist : BasePersist<IcAuditFault, ic_auditContext>
    {
        public AuditFaultPersist(DbContext context) : base(context)
        {

        }

        public List<IcAuditFault> GetAllAuditFault()
        {
            return GetContext().IcAuditFault.Where(it => it.RecordStatus == 100).ToList();
        }
    }
}
